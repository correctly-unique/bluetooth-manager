#!/bin/bash
PROJECT_DIR=/home/user/bluetooth-manager-flask;
source $PROJECT_DIR/env/bin/activate;
export FLASK_APP=$PROJECT_DIR/app.py;
export FLASK_ENV=production;
flask run --host=0.0.0.0 --port=8080;
