from flask import Flask, render_template
from flask import jsonify

from shlex import quote
from subprocess import Popen, PIPE

app = Flask(__name__)

STDBUF = 'stdbuf -oL'
TOOL = 'hcitool'
SCAN_COMMAND = 'scan'
INFO_COMMAND = 'info'

def get_command(cmd):
    return f'sudo {STDBUF} {TOOL} {cmd}'

def get_scan_command():
    return get_command(SCAN_COMMAND)

def get_info_command(address):
    return get_command(f'{INFO_COMMAND} {address}')

def run(cmd):
    out, err= Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE).communicate()
    return out.decode(), err.decode()

def remove_header(lst):
    if len(lst) == 1:
        return []
    return lst[1:]

def convert_list_to_object(lst):
    res = []
    for i in lst:
        i = i.strip().split(maxsplit=1)
        obj = {'addr':i[0], 'name':i[1]}
        res.append(obj)
    return res


def convert_to_list_and_remove_header(out, scan=False):
    out = out.strip(' ')\
        .strip('\n')\
        .replace('\t', ' '*4)\
        .split('\n')
    out = remove_header(out)
    if not scan:
        return out
    return convert_list_to_object(out)

def run_cmd_as_dict(cmd):
    out, err = run(cmd)
    return {'data': out, 'errors': err}

def make_response(cmd, scan=False):
    res = run_cmd_as_dict(cmd)
    res['data'] = convert_to_list_and_remove_header(res['data'], scan)
    return jsonify(**res)

@app.route('/scan/')
def scan_hcitool():
    cmd = get_scan_command()
    print('masuk scan command:', cmd)
    return make_response(cmd, scan=True)

@app.route('/info/<address>/')
def info_hcitool(address):
    address = quote(address)
    cmd = get_info_command(address)
    print('masuk info command:', cmd)
    return make_response(cmd)

@app.route('/')
def hello_world():
    return render_template('index.html')

