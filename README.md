# Bluetooth Manager
# System Programming 2020/2021

Team:
* 1806205110 Muhammad Ariq Basyar
* 1806141454 Sean Zeliq Urian
* 1806133793 Zafir Rasyidi Taufik

## Installation
### As a simple web app
> Prerequisites
* [Sysprog .ova](https://drive.google.com/open?id=1d-Ml_DDupAuJhVk-gWuqKydteNJhC5OF)
and follow the [instructions](https://projects.ui.ac.id/projects/kuliah-sysprog/wiki/Import_Virtual_Appliance)

* clone this repo with specific name

    ```shell
    git clone https://gitlab.com/correctly-unique/bluetooth-manager.git ~/bluetooth-manager-flask
    ```

* python3's [venv](https://docs.python.org/3/library/venv.html)

    ```shell
    python3 -m venv env
    ```

    > note that the venv name must be "env" or you have to change the config of
the [systemd service script](bluetooth-manager.sh)

* Activate the venv

    ```shell
    source env/bin/activate
    ```

* Install requirements

    ```shell
    pip install -r requirements.txt
    ```

* set the `FLASK_APP`

    ```shell
    FLASK_APP=app.py
    ```

* Run the flask app

    ```shell
    flask run --host=0.0.0.0 --port=8080
    ```

### As a Systemd Service
> (but you should complete the [As a simple web app](#as-a-simple-web-app)
section first until `install requirements`)

* change the `bluetooth-manager.sh` permission to executable

    ```shell
    chmod +x bluetooth-manager.sh
    ```

* copy the `bluetooth-manager.sh` to `/usr/bin/` folder


    ```shell
    sudo cp bluetooth-manager.sh /usr/bin/bluetooth-manager.sh
    ```

* change the `bluetooth-manager.service` permission to 644

    ```shell
    chmod 644 bluetooth-manager.service
    ```

* copy the `bluetooth-manager.service` to `/lib/systemd/system/` folder

    ```shell
    sudo cp bluetooth-manager.service /lib/systemd/system/bluetooth-manager.service
    ```

* start the service

    ```shell
    sudo systemctl start bluetooth-manager.service
    ```

* enable the service

    ```shell
    sudo systemctl enable bluetooth-manager.service
    ```

* check the service status

    ```shell
    sudo systemctl status bluetooth-manager.service
    ```

